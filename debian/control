Source: e-foto
Maintainer: Petter Reinholdtsen <pere@debian.org>
Standards-Version: 4.7.0
Priority: optional
Section: science
Build-Depends: debhelper-compat (= 13),
               imagemagick,
               libfontconfig1,
               libgdal-dev,
               libglu1-mesa-dev,
               libx11-xcb-dev,
               mesa-common-dev,
               qt5-qmake
Homepage: http://www.efoto.eng.uerj.br/
Vcs-Browser: https://salsa.debian.org/pere/e-foto
Vcs-Git: https://salsa.debian.org/pere/e-foto.git
Rules-Requires-Root: no

Package: e-foto
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Educational Digital Photogrammetry Workstation
 Beta version of E-foto. Contains some bugs that may affect your work.
 This is an academic initiative to develop free software for Digital
 Photogrammetry. The E-Foto project has been in development at the
 Photogrammetry Lab of the Rio de Janeiro State University’s School of
 Engineering since 2004.
 .
 The idea behind the E-Foto project was to offer a simple set of software
 (a free Digital Photogrammetric Workstation) that could help students
 understand the principles behind Photogrammetry, thus bypassing eventual
 costs that would have barred many from learning about Photogrammetry. This
 objective was reached through the development of free and user-friendly
 photogrammetric software.
 .
 This project is multidisciplinary. It involves several fields of knowledge,
 including Mathematical Modeling, Geodesy, Photogrammetry, and Software
 Engineering.
 .
 The project is based on two main principles (pillars): freedom and
 self-teaching. The final idea is to lead the students to fully understand
 the principles behind Photogrammetry by reading the e-book, using the
 software, taking a look at its source code, and even modifying it or
 developing new modules for it.
